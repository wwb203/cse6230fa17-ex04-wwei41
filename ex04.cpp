#include <iostream>
#include <random>
#include <chrono>
#include <stdlib.h>
#include <vector>
#include <sys/time.h>
#include <omp.h>
#include <math.h>
#include "forces.h"
const int N = 10000;//number of particles
const int T = 5;//number of time step
const int steps = 1; //print every 500 steps
const double fac = sqrt(2 * 1.0e-4);
const double dt = 1.e-4;
/* After Hager & Wellein, Listing 1.2 */
double get_walltime()
{
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp,&tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}

int main(int argc, char** argv) {
    unsigned num_threads = 1;
    if(argc > 1) {
        num_threads = atoi(argv[1]);
    }
    omp_set_num_threads(num_threads);
    //random generators
    std::random_device r;
    std::vector<std::mt19937> generators;
    for(size_t i = 0; i < num_threads; ++i) {
        generators.emplace_back(std::mt19937(i* 10));
       // generators.emplace_back(std::mt19937(r()));
    }


    std::vector<double> X0(N * 3);//initial position
    std::vector<double> avg_distance;
    double min_time  = 0.0;
    for(int  repeat = 0; repeat < 1; ++repeat){
        double time = get_walltime();
        //initialize X0
        #pragma omp parallel for schedule(static)
        for (int i = 0; i < N * 3; ++i) {
            //Get generator based on thread id
            std::mt19937& engine = generators[omp_get_thread_num()];
            //random uniform distsribution
            std::uniform_real_distribution<double> RU01(0,1.0);
            X0[i] = RU01(engine);
        }
        //X is coordinate that evolve over time
        std::vector<double> X(X0);
        //Force
        std::vector<double> F(N * 3);

        compute_forces(N, &X[0], &F[0]);
        for (int t = 0; t <= T; ++t) {
            #pragma omp parallel for schedule(static)
            for(int i = 0; i < 3 * N; ++i) {
                //Get generator based on thread id
                std::mt19937& engine = generators[omp_get_thread_num()];
                //random uniform distsribution
                std::uniform_real_distribution<double> RU(-1.0, 1.0);
                //apply noise
                X[i] += RU(engine)*fac;
                //apply force
                X[i] += F[i] * dt;
                //bring particle back in the unit box
                X[i] = remainder(X[i],1.0);
                if(X[i] < 0) X[i] += 1.0;
                if(X[i] > 1.0) X[i] -= 1.0;
            }
            //print results
            if(t % steps == 0) {
                double sum = 0.0;
            #pragma omp parallel for reduction(+:sum) schedule(static)
                for(int i = 0; i < 3 * N; i+=3) {
                    double r2 = 0;
                    for(int j = 0; j < 3; ++j)
                    {
                        double delta = X[i + j] - X0[i + j];
                        r2 += delta * delta;
                    }
                    sum += sqrt(r2);
                }
                avg_distance.emplace_back(sum / N);
            }
        }
        if(repeat == 0) {
            for(size_t i = 0; i < avg_distance.size(); ++i)
                std::cout <<  avg_distance[i] << ","<< std::endl;
            min_time = get_walltime() - time;
        } else {
            time = get_walltime() - time;
            min_time = time < min_time ? time : min_time;
        }
    }
    std::cout << min_time << std::endl;
    return 0;
}
