#include "forces.h"

static double a = 0.2; /* particle radius for this collision problem */

void compute_forces(int N, const double *pos, double *forces)
{
  int i;

  for (i = 0; i < 3 * N; i++) {
    forces[i] = 0.;
  }
}
