#include "forces.h"
#include <math.h>
#include <omp.h>

static double a = 0.2; /* particle radius for this collision problem */
static double d2_cut = 4.0 * a * a;
static double k = 100;
static double L = 1.0;

void compute_forces(int N, const double *pos, double *forces)
{
#pragma omp parallel for schedule(static)
  for (int i = 0; i < 3 * N; i++) {
    forces[i] = 0.;
  }
#pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < N; ++i) {

    for (int j = i + 1; j < N; ++j) {
      double dx = pos[i * 3] - pos[j * 3];
      //minium image distance
      dx = remainder(dx, L);
      double dy = pos[i * 3 + 1] - pos[j * 3 + 1];
      dy = remainder(dy, L);
      double dz = pos[i * 3 + 2] - pos[j * 3 + 2];
      dz = remainder(dz, L);
      double d2 = dx * dx + dy * dy + dz * dz;
      if(d2 < d2_cut) {
        double d = sqrt(d2);
        double f = k * (2 * a - d);
        forces[i * 3] += f * dx / d;
        forces[i * 3 + 1] += f * dy / d;
        forces[i * 3 + 2] += f * dz /d;

        #pragma omp critical
        {
          forces[j * 3] -= f * dx / d;
          forces[j * 3 + 1] -= f * dy / d;
          forces[j * 3 + 2] -= f * dz / d;
        }
      }


    }
  }
}
