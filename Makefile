
CC = g++
CFLAGS = -g -Wall -O3 -std=gnu++0x
OMPFLAGS = -fopenmp
RM = rm -f

all: ex04

%.o: %.cpp
	$(CC) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

ex04.o: forces.h
forces.o: forces.h

ex04: ex04.o forces.o
	$(CC) $(OMPFLAGS) -o $@ $?

clean:
	$(RM) -f *.o ex04
